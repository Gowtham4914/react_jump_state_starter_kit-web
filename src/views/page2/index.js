import React, { Component } from "react";

class page2 extends Component {
  // constructor (props) {
  //   super(props)
  // }

  // binding the router context
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  //  navigating dynamically
  navigateTo = path => {
    this.context.router.push({ pathname: path });
  };
  render() {
    return (
      <div className="web-page">
        <section className="row1">
          <div className="center">
            <div className="motion">Full Motion</div>
            <div className="function">
              A responsive video gallery template with a functional lightbox
              <br /> designed by Templated and released under the Creative
              Commons License.
            </div>
          </div>
        </section>
        <section className="row2">
          <div className="row2-1">
            <div className="img-col-1">
              <div className="img-1" />
              <div className="name-1">Nascetur nunc varius commodo</div>
              <div>
                <p className="info-1">
                  Interdum amet accumsan placerat commodo ut amet aliquam
                  blandit nunc tempor lobortis nunc non. Mi accumsan.
                </p>
              </div>
              <button className="button-1">watch</button>
            </div>
            <div className="img-col-2">
              <div className="img-2" />
              <div className="name-2">Nascetur nunc varius commodo</div>
              <div>
                <p className="info-2">
                  Interdum amet accumsan placerat commodo ut amet aliquam
                  blandit nunc tempor lobortis nunc non. Mi accumsan.
                </p>
              </div>
              <button className="button-2">watch</button>
            </div>
            <div className="img-col-3">
              <div className="img-3" />
              <div className="name-3">Nascetur nunc varius commodo</div>
              <div className="testing">
                <p>
                  Interdum amet accumsan placerat commodo ut amet aliquam
                  blandit nunc tempor lobortis nunc non. Mi accumsan.
                </p>
              </div>
              <button className="button-3">watch</button>
            </div>
          </div>
          <div className="row2-2">
            <div className="img-col-4">
              <div className="img-4" />
              <div className="name-4">Nascetur nunc varius commodo</div>
              <div>
                <p className="info-4">
                  Interdum amet accumsan placerat commodo ut amet aliquam
                  blandit nunc tempor lobortis nunc non. Mi accumsan.
                </p>
              </div>
              <button className="button-4">watch</button>
            </div>
            <div className="img-col-5">
              <div className="img-5" />
              <div className="name-5">Nascetur nunc varius commodo</div>
              <div>
                <p className="info-5">
                  Interdum amet accumsan placerat commodo ut amet aliquam
                  blandit nunc tempor lobortis nunc non. Mi accumsan.
                </p>
              </div>
              <button className="button-5">watch</button>
            </div>
            <div className="img-col-6">
              <div className="img-6" />
              <div className="name-6">Nascetur nunc varius commodo</div>
              <div>
                <p className="info-6">
                  Interdum amet accumsan placerat commodo ut amet aliquam
                  blandit nunc tempor lobortis nunc non. Mi accumsan.
                </p>
              </div>
              <button className="button-6">watch</button>
            </div>
          </div>

          <section className="row3">
            <div className="center-1">
              <div className="lorem">Etiam veroeros lorem</div>
              <p className="copy">
                Pellentesque eleifend malesuada efficitur. Curabitur volutpat
                dui mi, ac imperdiet dolor tincidunt nec. Ut erat lectus, dictum
                sit amet lectus a, aliquet rutrum mauris. Etiam nec lectus
                hendrerit, consectetur risus viverra, iaculis orci. Phasellus eu
                nibh ut mi luctus auctor. Donec sit amet dolor in diam feugiat
                venenatis.
              </p>
              <div className="logo">
                <i className="fa fa-twitter-square" aria-hidden="true" />
                <i className="fa fa-facebook-official" aria-hidden="true" />
                <i className="fa fa-instagram" aria-hidden="true" />
                <i className="fa fa-envelope" aria-hidden="true" />
              </div>
              <div className="copyright">
                © Untitled. Design: TEMPLATED. Images: Unsplash. Videos: Coverr.
              </div>
            </div>
          </section>
        </section>
      </div>
    );
  }
}

export default page2;
