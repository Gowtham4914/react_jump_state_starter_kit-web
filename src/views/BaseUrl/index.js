import React, { Component } from "react";
import { BASE_URL } from "../../utils/baseURL";
import * as URL from "../../utils/URL";

export default class BaseUrl extends Component {
    constructor() {
        super();
        this.state = {
            baseURL: URL.getBaseURL()
        };
    }

    submitURL = e => {
        e.preventDefault();
        let url = this.refs.baseURL.value || BASE_URL;
        this.setUrl(url);
    };
    resetUrl = e => {
        e.preventDefault();
        console.log(BASE_URL);
        this.setUrl(BASE_URL);
    };
    setUrl = url => {
        URL.setBaseURL(url);
        this.setState({ baseURL: url });
    };
    render() {
        return (
            <div>
                <br />
                <label>
                    Base url:{" "}
                    <span className="alert alert-info" role="alert">
                        {this.state.baseURL}
                    </span>
                </label>
                <form onSubmit={this.submitURL}>
                    <input type="text" ref="baseURL" required />
                    <button className="btn btn-info btn-sm" type="submit">
                        Submit
                    </button>
                    &nbsp;
                    <button
                        onClick={this.resetUrl}
                        className="btn btn-danger btn-sm"
                    >
                        Reset
                    </button>
                </form>
            </div>
        );
    }
}
