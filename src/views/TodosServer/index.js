import React, { Component } from "react";
import { connect } from "react-redux";
import TodoFooter from "../../components/Todos/TodoFooter";
import TodoItem from "../../components/Todos/TodoItem";
import { Actions } from "jumpstate";

console.log(TodoFooter);

const mapStateToProps = state => {
    // getting todos from global state
    return {
        todos: state.todosV1.details
    };
};
const mapDispatchToProps = dispatch => {};

class TodosServer extends Component {
    // get initial state is replaced with constructor in ES6
    constructor(props, context) {
        super(props, context);
        this.state = {
            newTodo: ""
        };
    }
    componentDidMount() {
        // calling Api endpoint to get list of todos
        if (this.props.todos.length === 0) {
            Actions.fetchTodos();
        }
    }
    // this is how we bind the function to component class in ES6
    // function_name = (prams1, params2) => { ...logic }
    handleChange = event => {
        this.setState({
            newTodo: event.target.value
        });
    };
    handleAddTodo = event => {
        const ENTER_KEY = 13;
        if (event.keyCode !== ENTER_KEY) {
            return;
        }
        // accessing the object properties as variables in ES6
        let { newTodo } = this.state;
        let todoText = newTodo.trim();
        if (todoText) {
            // Adding todos in server using network calls
            let requestObject = {
                description: todoText,
                is_completed: false,
                remind_at: "2017-03-05T05:10:33Z"
            };
            Actions.createTodo(requestObject);
            this.setState({ newTodo: "" });
        }
    };

    onToggle = (todoId, status) => {
        // implent it
    };

    clearCompleted = () => {
        Actions.clearCompleted();
    };
    showTodos = () => {
        let { todos } = this.props;
        let todoItems = null;
        // for every map function we need pass one unique key value as prop to child
        todoItems = todos.map((todoItem, index) => {
            return (
                <TodoItem
                    key={index}
                    todo={todoItem}
                    toggle={this.onToggle}
                    deleteTodo={Actions.deleteTodo}
                    onDestroy={this.destroy}
                />
            );
        });
        return todoItems;
    };
    render() {
        let { newTodo } = this.state;
        return (
            <div className="container col-md-6 col-md-offset-3">
                <div className="alert alert-info" role="alert">
                    Todos
                </div>
                <section className="todoapp">
                    <div>
                        <header className="header">
                            <input
                                className="new-todo"
                                placeholder="Add a Todo"
                                value={newTodo}
                                onKeyDown={this.handleAddTodo}
                                onChange={this.handleChange}
                                autoFocus
                            />
                        </header>
                        <ul className="list-group">
                            {/* calling the functions */}
                            {this.showTodos()}
                        </ul>
                        <TodoFooter onClearCompleted={this.clearCompleted} />
                    </div>
                </section>
                <div className="footer-msg">
                    press `ctrl + h` to open devtools
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosServer);
