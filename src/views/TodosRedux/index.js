import React, { Component } from "react";
import { connect } from "react-redux";
import TodoFooter from "../../components/Todos/TodoFooter";
import TodoItem from "../../components/Todos/TodoItem";
import { Actions } from "jumpstate";

// Binding the state and actions. These will be available as props to component
const mapStateToProps = state => {
    // getting todos from global state
    return {
        todos: state.todos
    };
};
const mapDispatchToProps = dispatch => {};

// React Component
// merging the required global state as props
// we can aaccess todos as `this.props.todos` thi TodoApp Component

class TodosRedux extends Component {
    // get initial state is replaced with constructor in ES6
    constructor(props, context) {
        super(props, context);
        this.state = {
            newTodo: ""
        };
    }
    static contextTypes = {
        router: React.PropTypes.object
    };
    // this is how we bind the function to component class in ES6
    // function_name = (prams1, params2) => { ...logic }
    handleChange = event => {
        this.setState({
            newTodo: event.target.value
        });
    };
    handleAddTodo = event => {
        const ENTER_KEY = 13;
        if (event.keyCode !== ENTER_KEY) {
            return;
        }
        // accessing the object properties as variables in ES6
        let { newTodo } = this.state;
        let todoText = newTodo.trim();
        if (todoText) {
            let payload = { todoText };
            Actions.TodoState.addTodo(payload);
            this.setState({ newTodo: "" });
        }
    };
    clearCompleted = () => {
        Actions.TodoState.clearCompleted();
    };
    onToggle = (todoId, status) => {
        let payload = { todoId, status };
        Actions.TodoState.toggleTodo(payload);
    };
    showTodos = () => {
        let { todos } = this.props;
        let todoItems = null;
        // for every map function we need to pass
        // one unique key value as prop to child
        todoItems = todos.map((todoItem, index) => {
            return (
                <TodoItem
                    key={index}
                    todo={todoItem}
                    toggle={this.onToggle}
                    deleteTodo={Actions.TodoState.removeTodo}
                />
            );
        });
        return todoItems;
    };
    render() {
        let { newTodo } = this.state;
        return (
            <div className="container col-md-6 col-md-offset-3">
                <div className="alert alert-info" role="alert">
                    Todos
                </div>
                <section className="todoapp">
                    <div>
                        <header className="header">
                            <input
                                className="new-todo"
                                placeholder="Add a Todo"
                                value={newTodo}
                                onKeyDown={this.handleAddTodo}
                                onChange={this.handleChange}
                                autoFocus
                            />
                        </header>
                        <ul className="list-group">
                            {/* calling the functions */}
                            {this.showTodos()}
                        </ul>
                        <TodoFooter onClearCompleted={this.clearCompleted} />
                    </div>
                </section>
                <div className="footer-msg">
                    press `ctrl + h` to open devtools
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosRedux);
