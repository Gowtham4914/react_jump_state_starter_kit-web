import React, { Component } from "react";

class Page1 extends Component {
  // constructor (props) {
  //   super(props)
  // }

  // binding the router context
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  //  navigating dynamically
  navigateTo = path => {
    this.context.router.push({ pathname: path });
  };
  render() {
    return (
      <div>
        <div className="web-page">
          <section className="google-keep">
            <div className="upper-part">
              <span className="google">Google Keep</span>
              <div className="logo">
                <i className="fa fa-google-plus" aria-hidden="true" />
                <i className="fa fa-facebook" aria-hidden="true" />
                <i className="fa fa-twitter" aria-hidden="true" />
              </div>
            </div>
            <div className="thoughts-part">
              <p className="heading">
                Save your thoughts, <br />wherever you are
              </p>
              <button className="button-1">Try Google Keep</button>
            </div>
          </section>
          <section className="capture">
            <div className="notes">
              <p className="Capture-mind">Capture what’s on your mind</p>
              <p className="photo-note">
                Add notes, lists, photos, and audio to Keep.
              </p>
            </div>
            <div className="row-1">
              <div className="imgs-1">
                <div className="col-1">
                  <p className="party">
                    Surprise<br /> party for <br />Kristen!
                  </p>
                  <div className="date-time-1">
                    <i className="material-icons">alarm</i>
                    <p className="time-1">Reminder at 8:00 PM, Jun 19</p>
                  </div>
                </div>
                <div className="icon">
                  <i className="fa fa-align-justify" aria-hidden="true" />
                </div>
              </div>

              <div className="imgs-2">
                <div className="col-2">
                  <p className="ideas">Gift ideas</p>
                  <div className="iteams">
                    <div className="checks">
                      <p />
                      <span className="bikes"> New bike helmet</span>
                    </div>
                    <div className="checks">
                      <p />
                      <span className="candles"> Candles? </span>
                    </div>
                    <div className="checks">
                      <p />
                      <span className="plants"> Cute house plant? </span>
                    </div>
                    <div className="checks">
                      <p />
                      <span className="drop"> Drop earrings </span>
                    </div>
                  </div>
                  <div className="dotted">...</div>
                  <div className="loaction">
                    <i className="material-icons">location_on</i>
                    <p className="adress">Reminder at 8th Street Market</p>
                  </div>
                </div>
                <div className="icon">
                  <i className="material-icons">list</i>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default Page1;
