import React, { Component } from "react";
import HelloWorld from "./components/HelloWorld/";
import Footer from "./components/Footer/";

export default class Home extends Component {
    render() {
        return (
            <div className="container">
                <div className="container">
                    <div className="jumbotron">
                        <h1>
                            Welcome to Tutorial
                        </h1>
                        <p>
                            Let's Start our Todo App
                        </p>
                    </div>
                </div>
                <HelloWorld />
                <Footer />
            </div>
        );
    }
}
