import React, { Component } from "react";
import TodoFooter from "../../components/Todos/TodoFooter";
import TodoItem from "../../components/Todos/TodoItem";

class Todos extends Component {
    // get initial state is replaced with constructor in ES6
    constructor(props, context) {
        super(props, context);
        // defining the state
        this.state = {
            newTodo: "",
            todos: [
                {
                    id: -1,
                    description: "Hello World",
                    is_completed: false,
                    remind_at: "2017-03-05T05:10:33Z"
                },
                {
                    id: -2,
                    description: "I am from local state of component",
                    is_completed: false,
                    remind_at: "2017-03-05T05:10:33Z"
                }
            ]
        };
    }
    // this is how we bind the function to component class in ES6
    // function_name = (prams1, params2) => { ...logic }
    handleChange = event => {
        this.setState({
            newTodo: event.target.value
        });
    };
    // actions to update state
    handleAddTodo = event => {
        console.log("here");
        const ENTER_KEY_CODE = 13;
        if (event.keyCode !== ENTER_KEY_CODE) {
            return;
        }
        // accessing the object properties as variables in ES6
        let { newTodo, todos } = this.state;
        console.log(newTodo, todos);
        let todoText = newTodo.trim();
        if (todoText) {
            let todoId = todos.length + 1;
            let todo = {
                id: todoId,
                description: todoText,
                is_completed: false
            };
            // adding new todo to array and forming a new state (list of todos)
            todos = [...todos, todo];
            console.log("todos", todos);
            // updating the state
            this.setState({
                todos: todos,
                newTodo: ""
            });
        }
    };

    // actions to update state
    clearCompleted = () => {
        let { todos } = this.state;
        todos = todos.filter((todo, index) => {
            return !todo.is_completed;
        });
        this.setState({
            todos: todos
        });
    };

    // actions to update state
    onToggle = (todoId, status) => {
        let { todos } = this.state;
        todos.map((todo, index) => {
            if (todo.id === todoId) {
                // updating the todo status
                todos[index].is_completed = status;
            }
        });
        this.setState({
            todos: todos
        });
    };

    // function to display todos list
    showTodos = () => {
        // accessing todos from state
        let { todos } = this.state;
        let todoItems = null;
        // for every map function we need to pass
        // one unique key value as prop to child
        // example of passing props to compontnts
        todoItems = todos.map((todoItem, index) => {
            return (
                <TodoItem
                    key={index}
                    todo={todoItem}
                    toggle={this.onToggle}
                    deleteTodo={this.removeTodo}
                />
            );
        });
        return todoItems;
    };
    render() {
        let { newTodo } = this.state;
        return (
            <div className="container col-md-6 col-md-offset-3">
                <div className="alert alert-info" role="alert">
                    Todos
                </div>
                <section className="todoapp">
                    <div>
                        <header className="header">
                            <input
                                className="new-todo"
                                placeholder="Add a Todo"
                                value={newTodo}
                                onKeyDown={this.handleAddTodo}
                                onChange={this.handleChange}
                                autoFocus
                            />
                        </header>
                        <ul className="list-group">
                            {/* calling the functions */}
                            {this.showTodos()}
                        </ul>
                        <TodoFooter onClearCompleted={this.clearCompleted} />
                    </div>
                </section>
                <div className="footer-msg">
                    press `ctrl + h` to open devtools
                </div>
            </div>
        );
    }
}

export default Todos;
