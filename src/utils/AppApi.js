import { getAccessToken, getError } from "utils/AppUtils";
import { BASE_URL } from "./baseURL";
import { Actions } from "jumpstate";
import { isLoggedIn } from "./AppUtils";

import $ from "jquery";
var url;
var requestObject;
var onSuccess;
var onFailure;
var type;
var count;
var maxRetries = 2;

// generic web api call to make network requests
function genericWebAPICall(
    url,
    requestObject,
    onSuccess,
    onFailure,
    type = "POST",
    currentState = null,
    count = 1,
    choice = false
) {
    url = url;
    // console.log(url)
    requestObject = requestObject;
    onSuccess = onSuccess;
    onFailure = onFailure;
    type = type;
    console.log(url);
    if (count < 2) {
        console.log("RequestObject:", requestObject);
        if (type === "POST") {
            const formattedrequestObject = JSON.stringify(
                JSON.stringify(requestObject)
            );
            const dataRequestObject = JSON.stringify({
                data: formattedrequestObject,
                clientKeyDetailsId: 1
            });
            console.log("dataRequestObject >>> ", dataRequestObject);
            let accessToken = isLoggedIn();
            // let accessToken = "todo_app";
            if (currentState) {
                Actions[currentState].updateStatus(100);
            }
            $.ajax({
                type: "POST",
                url: url,
                data: dataRequestObject,
                contentType: "application/json",
                beforeSend: function(xhr) {
                    if (accessToken !== "" || accessToken !== null) {
                        xhr.setRequestHeader(
                            "Authorization",
                            "Bearer " + accessToken
                        );
                    }
                }
            })
                .done(function(response) {
                    // console.log(response)
                    onSuccess(response);
                    if (currentState) {
                        Actions[currentState].updateStatus(200);
                    }
                })
                .fail(function(response) {
                    console.log(response);
                    if (count === 1) {
                        onFailure(response);
                        let error_message = getError(response);
                        if (currentState) {
                            Actions[currentState].updateStatus(400);
                            Actions[currentState].updateErrors(error_message);
                        }
                    } else {
                        genericWebAPICall(
                            url,
                            requestObject,
                            onSuccess,
                            onFailure,
                            type,
                            currentState,
                            count + 1
                        );
                    }
                });
        }
        if (type === "PUT") {
            const formattedrequestObject = JSON.stringify(
                JSON.stringify(requestObject)
            );
            const dataRequestObject = JSON.stringify({
                data: formattedrequestObject,
                clientKeyDetailsId: 1
            });
            console.log("dataRequestObject >>> ", dataRequestObject);
            let accessToken = isLoggedIn();
            // let accessToken = "todo_app";
            if (currentState) {
                Actions[currentState].updateStatus(100);
            }
            $.ajax({
                type: "PUT",
                url: url,
                data: dataRequestObject,
                contentType: "application/json",
                beforeSend: function(xhr) {
                    if (accessToken !== "" || accessToken !== null) {
                        xhr.setRequestHeader(
                            "Authorization",
                            "Bearer " + accessToken
                        );
                    }
                }
            })
                .done(function(response) {
                    // console.log(response)
                    onSuccess(response);
                    if (currentState) {
                        Actions[currentState].updateStatus(200);
                    }
                })
                .fail(function(response) {
                    // console.log(response)
                    if (count === 1) {
                        onFailure(response);
                        let error_message = getError(response);
                        if (currentState) {
                            Actions[currentState].updateStatus(400);
                            Actions[currentState].updateErrors(error_message);
                        }
                    } else {
                        genericWebAPICall(
                            url,
                            requestObject,
                            onSuccess,
                            onFailure,
                            type,
                            count + 1
                        );
                    }
                });
        } else if (type === "GET") {
            let accessToken = isLoggedIn();
            // let accessToken = "todo_app";
            if (currentState) {
                Actions[currentState].updateStatus(100);
            }
            $.ajax({
                type: "GET",
                url: url,
                data: requestObject,
                contentType: "application/json",
                beforeSend: function(xhr) {
                    if (accessToken !== "" || accessToken !== null) {
                        xhr.setRequestHeader(
                            "Authorization",
                            "Bearer " + accessToken
                        );
                    }
                }
            })
                .done(function(response) {
                    // console.log(response)
                    onSuccess(response);
                    if (currentState) {
                        Actions[currentState].updateStatus(200);
                    }
                })
                .fail(function(response) {
                    if (count === 1) {
                        onFailure(response);
                        let error_message = getError(response);
                        if (currentState) {
                            Actions[currentState].updateStatus(400);
                            Actions[currentState].updateErrors(error_message);
                        }
                    } else {
                        genericWebAPICall(
                            url,
                            requestObject,
                            onSuccess,
                            onFailure,
                            type,
                            count + 1
                        );
                    }
                });
        } else if (type === "DELETE") {
            let accessToken = isLoggedIn();
            // let accessToken = "todo_app";
            if (currentState) {
                Actions[currentState].updateStatus(100);
            }
            $.ajax({
                type: "DELETE",
                url: url,
                beforeSend: function(xhr) {
                    if (accessToken !== "" || accessToken !== null) {
                        xhr.setRequestHeader(
                            "Authorization",
                            "Bearer " + accessToken
                        );
                    }
                }
            })
                .done(function(response) {
                    // console.log(response)
                    onSuccess(response);
                    if (currentState) {
                        Actions[currentState].updateStatus(200);
                    }
                })
                .fail(function(response) {
                    if (count === 1) {
                        onFailure(response);
                        let error_message = getError(response);
                        if (currentState) {
                            Actions[currentState].updateStatus(400);
                            Actions[currentState].updateErrors(error_message);
                        }
                    } else {
                        genericWebAPICall(
                            url,
                            requestObject,
                            onSuccess,
                            onFailure,
                            type,
                            count + 1
                        );
                    }
                });
        }
    }
}

export { genericWebAPICall };
