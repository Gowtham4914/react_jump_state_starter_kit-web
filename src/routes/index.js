import React from "react";
import { Route, IndexRoute } from "react-router";

import CoreLayout from "../layouts/CoreLayout";

import BaseUrl from "../views/BaseUrl";
import Home from "../views/Home";
import Todos from "../views/Todos";
import Page1 from "../views/Page1";
import TodosRedux from "../views/TodosRedux";
import TodosServer from "../views/TodosServer";

import * as utils from "../utils/AppUtils";

function requireAuth(location, replace) {
  if (!utils.getCookie("access_token")) {
    replace("/");
  }
}
// bind the view components to appropriate URL path
export default store =>
  <div>
    <Route path="/" component={CoreLayout}>
      <IndexRoute component={Home} />
      <Route path="home" component={Home} />
      <Route path="todos" component={Todos} />
      <Route path="Page1" component={Page1} />
      <Route path="todos-global" component={TodosRedux} />
      <Route path="todos-server" component={TodosServer} />
      <Route path="url" onEnter={requireAuth} component={BaseUrl} />
    </Route>
  </div>;
