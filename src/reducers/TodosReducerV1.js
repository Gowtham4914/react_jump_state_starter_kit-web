import { State, Effect } from "jumpstate";
import { genericWebAPICall } from "../utils/AppApi";
import { getBaseURL } from "../utils/URL";
import { Actions } from "jumpstate";

const currentState = "TodoStateV1";
export default State(currentState, {
    // Initial State should be starts with the key 'initial': ...
    initial: {
        apiStatus: 0,
        details: [],
        errors: {}
    },

    updateStatus(state, payload) {
        state.apiStatus = payload;
        return { ...state };
    },
    updateErrors(state, payload) {
        state.errors = payload;
        return { ...state };
    },
    // Action 1
    getTodos(state, payload) {
        // merging the new todos to todos list
        let todos = [...state.details, ...payload];
        state.details = todos;
        return { ...state };
    },
    // Action 2
    addTodo(state, todo) {
        state.details = [...state.details, todo];
        return { ...state };
    },
    // Action 3
    removeTodo(state, todoId) {
        console.log("here");
        try {
            let todoIndex = state.details.findIndex(todo => todo.id === todoId);
            state.details.splice(todoIndex, 1);
        } catch (error) {
            console.log(error);
        }
        return { ...state };
    }
});

// Network calls
Effect("fetchTodos", (requestObject = {}) => {
    let apiEndpoint = getBaseURL() + "api/todos_v2/todos/";
    let requestType = "GET";
    console.log(genericWebAPICall, apiEndpoint, requestType);
    genericWebAPICall(
        apiEndpoint,
        requestObject,
        function(response) {
            console.log(response);
            // call the action to update the state
            Actions.TodoStateV1.getTodos(response);
        },
        function(response) {
            console.log(response);
        },
        requestType,
        currentState
    );
});

// Network calls
Effect("createTodo", (requestObject = {}) => {
    let apiEndpoint = getBaseURL() + "api/todos_v2/todos/";
    let requestType = "POST";
    console.log(genericWebAPICall, apiEndpoint, requestType);
    genericWebAPICall(
        apiEndpoint,
        requestObject,
        function(response) {
            console.log(response);
            let todo = { ...response, ...requestObject };
            // call the action to update the state
            Actions.TodoStateV1.addTodo(todo);
        },
        function(response) {
            console.log(response);
        },
        requestType,
        currentState
    );
});

// Network calls
Effect("deleteTodo", todoId => {
    let apiEndpoint = getBaseURL() + `api/todos_v2/todos/${todoId}/`;
    let requestType = "DELETE";
    let requestObject = {};
    console.log(genericWebAPICall, apiEndpoint, requestType);
    genericWebAPICall(
        apiEndpoint,
        requestObject,
        function(response) {
            console.log(response);
            // call the action to update the state
            Actions.TodoStateV1.removeTodo(todoId);
        },
        function(response) {
            console.log(response);
        },
        requestType,
        currentState
    );
});
