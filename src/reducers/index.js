import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import TodosReducer from "./TodosReducer";
import TodosReducerV1 from "./TodosReducerV1";

export default combineReducers({
    routing: routerReducer,
    todos: TodosReducer,
    todosV1: TodosReducerV1
});
