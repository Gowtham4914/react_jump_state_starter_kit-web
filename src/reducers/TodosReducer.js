import { State } from "jumpstate";

// utility function to re arrange the todos
const rearrangeTodos = todos => {
    let completed = todos.filter((todo, index) => {
        return todo.is_completed;
    });
    let noCompleted = todos.filter((todo, index) => {
        return !todo.is_completed;
    });
    return [...noCompleted, ...completed];
};

const currentState = "TodoState";
export default State(currentState, {
    // Initial State should be starts with the key 'initial': ...
    initial: [
        {
            id: -1,
            description: "Hello World",
            is_completed: false,
            remind_at: "2017-03-05T05:10:33Z"
        },
        {
            id: -2,
            description: "My First Todo",
            is_completed: false,
            remind_at: "2017-03-05T05:10:33Z"
        }
    ],

    // Action 1
    addTodo(state, payload) {
        let { todoText } = payload;
        let todoId = state.length + 1;
        let todo = {
            id: todoId,
            description: todoText,
            is_completed: false
        };
        // appending the todos to existing todos state
        let todos = [...state, todo];
        todos = rearrangeTodos(todos);
        return todos;
    },
    // Action 2
    toggleTodo(state, payload) {
        // copy cureent state to another, we nedd to update the referece
        let todos = [...state];
        let { todoId, status } = payload;
        todos.map((todo, index) => {
            if (todo.id === todoId) {
                // updating the todo status
                todos[index].is_completed = status;
            }
        });
        todos = rearrangeTodos(todos);
        return todos;
    },
    // Action 3
    clearCompleted(state, payload) {
        // ES6 arrow functions
        return state.filter((todo, index) => {
            return !todo.is_completed;
        });
    },
    removeTodo(state, todoId) {
        let todoIndex = state.findIndex(todo => todo.id === todoId);
        state.splice(todoIndex, 1);
        let todos = rearrangeTodos(state);
        return todos;
    }
});
