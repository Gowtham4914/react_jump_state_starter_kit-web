import React, { Component } from "react";

class TodoItem extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    onToggle = () => {
        let { id, is_completed } = this.props.todo;
        let { toggle } = this.props;
        toggle(id, !is_completed);
    };
    render() {
        let { deleteTodo, todo } = this.props;
        // accessing object properties as variables
        let { id, description, is_completed } = todo;
        // console.log(id, description, is_completed)
        return (
            <li className="list-group-item">
                <div onClick={this.onToggle}>
                    <input
                        className="toggle"
                        type="checkbox"
                        checked={is_completed}
                        style={{ cursor: "pointer" }}
                    />
                    &nbsp;
                    <label
                        style={{
                            cursor: "pointer",
                            textDecoration: is_completed
                                ? "line-through"
                                : "initial"
                        }}
                    >
                        {description}
                    </label>
                    <button
                        className="btn btn-danger btn-sm pull-right"
                        onClick={() => deleteTodo(id)}
                    >
                        <i className="fa fa-trash-o" aria-hidden="true" />
                    </button>
                </div>
            </li>
        );
    }
}

// defining propTypes for ease of debugging
TodoItem.propTypes = {
    todo: React.PropTypes.object.isRequired,
    toggle: React.PropTypes.func.isRequired
};

export default TodoItem;
