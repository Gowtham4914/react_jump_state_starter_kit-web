import React from "react";

// this is a stateless functional component, ES6 arrow functions
const TodoFooter = props => {
    return (
        <footer className="footer">
            <button
                className="clear-completed"
                onClick={props.onClearCompleted}
            >
                Clear completed
            </button>
        </footer>
    );
};

export default TodoFooter;
