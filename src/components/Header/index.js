import React, { Component } from "react";
import { Link } from "react-router";
import { logo } from "../../static/assets/";
import { Nav, Navbar, NavItem, NavDropdown, MenuItem } from "react-bootstrap";

export default class Header extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = { showModal: false };
    }

    close = () => {
        this.setState({ showModal: false });
    };

    open = () => {
        this.setState({ showModal: true });
    };
    render() {
        console.log("Hai");
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="https://www.ibhubs.co" target="_blank">
                            <img
                                style={{ width: "30px" }}
                                className="img img-responsive"
                                alt="iB Hubs!"
                                src="https://d1ov0p6puz91hu.cloudfront.net/logos/iBhubs_logo.svg"
                            />
                        </a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">
                            Home
                            <span className="sr-only">(current)</span>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/todos" className="nav-link">
                            {" "}Todos{" "}
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/todos-global" className="nav-link">
                            {" "}Todos-Global{" "}
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/todos-server" className="nav-link">
                            {" "}Todos - Server{" "}
                        </Link>
                    </li>
                    <li className="nav-item">
                        <a
                            href="http://ibhubs-tutorials.s3-website-eu-west-1.amazonaws.com/react/"
                            target="_blank"
                            className="nav-link"
                        >
                            {" "}Tutorial{" "}
                        </a>
                    </li>
                    <li className="nav-item">
                        <Link to="/url" className="nav-link">
                            {" "}URL{" "}
                        </Link>
                    </li>
                    <li
                        className="nav-item"
                        onClick={() => {
                            document.cookie = "access_token=todo_app";
                        }}
                    >
                        <Link>Authenticate</Link>
                    </li>
                </Nav>
            </Navbar>
        );
    }
}
